namespace FizzBuzz.Types
{
    public class Footer: IFooter
    {
        public string Item{get;set;}
        public int ItemCount{get;set;}
    }
}