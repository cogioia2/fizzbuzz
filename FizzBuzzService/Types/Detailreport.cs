using System.Collections.Generic;
namespace FizzBuzz.Types
{
    public class DetailReport:IDetailReport
    {
        public List<IDetail> Details { get; set; }
        public DetailFooter Footer
        {
            get;set;
        }

    }
}