using System.Collections.Generic;
namespace FizzBuzz.Types
{
    public class DetailFooter: IDetailFooter
    {
        public List<IFooter> FooterList { get; set; }

    }
}