namespace FizzBuzz.Types
{
    public class Detail :IDetail
    {
        public int DetailNumber { get; set; }
        public string Label { get; set; }

    }
}