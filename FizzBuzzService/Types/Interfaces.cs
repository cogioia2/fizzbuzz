using System.Collections.Generic;

namespace FizzBuzz.Types
{
    public interface IDetail
    {
        int DetailNumber { get; set; }
        string Label { get; set; }
    }
    public interface IDetailFooter
    {
        List<IFooter> FooterList { get; set; }
    }
    public interface IDetails
    {
        List<IDetail> Details { get; set; }
        DetailFooter Footer
        {
            get; set;
        }

    }
    public interface IFooter
    {
        string Item { get; set; }
        int ItemCount { get; set; }
    }
    public interface IDetailReport
    {
        List<IDetail> Details { get; set; }
        DetailFooter Footer
        {
            get; set;
        }
    }
    public interface IDetailService
    {
        IDetailReport GetReport(IEnumerable<int> NumberList);


    }
    public interface IReportingService
    {
        bool OutputReport(OutputTypes outputType, IDetailReport report);
    }
}