﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzBuzz.Services;
using FizzBuzz.Types;


namespace FizzBuzz
{
    class Program
    {


        static void Main(string[] args)
        {
            try
            {
                //10,23,8,77,16,65,101,200,5,8,9,61,32,87,49,7
                DetailService detailService = new DetailService();
                ReportingService reportingService = new ReportingService();

                Console.WriteLine("Enter a comma delimetered list of numbers to report:");
                string detailList = Console.ReadLine();

                var dl = detailList.Split(',').Select(int.Parse).ToArray();

                var report = detailService.GetReport(dl);

                if(!reportingService.OutputReport(OutputTypes.Console,report))
                {
                    Console.WriteLine("An error occured. Please check the log for details");
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(String.Format("An error occured: {0}", ex.Message));
                //Write to log

            }
            
        }


    }

}
