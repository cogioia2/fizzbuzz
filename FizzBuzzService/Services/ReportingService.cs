using System;
using FizzBuzz.Types;


namespace FizzBuzz.Services
{
public class ReportingService : IReportingService
{

    public bool OutputReport(OutputTypes outputType, IDetailReport report)
        {
            try{
            switch (outputType)
            {
                case OutputTypes.Console:
                {
                    foreach(var r in report.Details)
                    {
                        Console.WriteLine(string.Format("{0} {1}",r.DetailNumber, r.Label));
                    }
                    
                    foreach(var f in report.Footer.FooterList)
                    {
                        Console.WriteLine(string.Format("{0}: {1}",f.Item, f.ItemCount));
                    }
                    break;
                }
                default:
                {
                    throw new System.Exception("Not Implemented");
                }
            }
            }
            catch(Exception)
            {
                throw;
            }

            return true;

        }
}
}
