using System.Collections.Generic;
using System.Linq;
using FizzBuzz.Types;
using System;

namespace FizzBuzz.Services
{
    public class DetailService : IDetailService
    {
        const int FizzBuzzM = 15;
        const int FizzM = 3;
        const int BuzzM = 5;
        const int LuckyM = 3;
        const string FizzBuzzL = "FizzBuz";
        const string FizzL = "Fizz";
        const string BuzzL = "Buzz";
        const string LuckyL = "Lucky";
        const string IntegerL = "Integer";

        public IDetailReport GetReport(IEnumerable<int> NumberList)
        {

            List<IDetail> detailList = new List<IDetail>();
            IDetailReport detailReport = new DetailReport();

            int FizzBuzzcount = 0, fizzcount = 0, buzzcount = 0, luckycount = 0, intergercount = 0;

            try
            {
            // Loop through number list and print correct label
            foreach (int x in NumberList)
            {
                if (x == LuckyM)
                {
                    luckycount++;
                    detailList.Add(new Detail { DetailNumber = x, Label = LuckyL });
                }
                else if (x % FizzBuzzM == 0)
                {
                    FizzBuzzcount++;
                    detailList.Add(new Detail { DetailNumber = x, Label = FizzBuzzL });
                }
                else if (x % FizzM == 0)
                {
                    fizzcount++;
                    detailList.Add(new Detail { DetailNumber = x, Label = FizzL });
                }
                else if (x % BuzzM == 0)
                {
                    buzzcount++;
                    detailList.Add(new Detail { DetailNumber = x, Label = BuzzL });
                }
                else
                {
                    intergercount++;
                }
            }

            detailReport.Details = detailList;
            detailReport.Footer = new DetailFooter
            {
                FooterList = new List<IFooter>() {
                new Footer {Item="Buzz",ItemCount=buzzcount},
                new Footer {Item="Fizz",ItemCount=fizzcount},
                new Footer {Item="FizzBuzz",ItemCount=FizzBuzzcount},
                new Footer {Item="Lucky",ItemCount=luckycount},
                new Footer {Item="Integer",ItemCount=intergercount}
            }
            };
            }
            catch(Exception)
            {
                throw;
            }

            return detailReport;
        }
        
    }
}