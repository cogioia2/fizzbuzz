using System;
using Xunit;
using FizzBuzz.Services;
using FizzBuzz.Types;


namespace FizzBuzzTests
{
    public class UnitTests
    {
        [Fact]
        public void GetReportTest()
        {
            var rservice = new ReportingService();
            var dservice = new DetailService();

            var test = dservice.GetReport(new int[]{1,7,9,10,48});

            Assert.True(test.Details.Count>0);


        }
    }
}
